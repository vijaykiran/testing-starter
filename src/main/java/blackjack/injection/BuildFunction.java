package blackjack.injection;

/**
 * The interface for no-arg build functions.
 * @param <T> The kind of thing that this function builds.
 */
public interface BuildFunction<T> {
    /**
     * Create a function that instantiates an instance of the given class the first time it is called, and memoizes that
     * instance for the duration of the BuildFunction's lifetime. This is an important pattern when creating interdependent
     * dependencies where construction order and declaration order may cause compilation problems. By deferring the construction
     * of a given dependency until it is used you can more easily declare interrelated dependencies that have complex
     * dependency relationships (including loops).
     *
     * @param clz The class of the item to build. It must have a no-arg constructor.
     * @param <S> The type being constructed.
     * @return A thread-safe BuildFunction that will construct and memoize a singleton instance.
     */
    static <S> BuildFunction singletonBuilder(Class<? extends S> clz) {
        return new BuildFunction() {
            private S value = null;

            @Override
            public synchronized S build() {
                if (value != null) return value;
                try {
                    value = clz.newInstance();
                    return value;
                } catch (Exception e) {
                    System.err.println("FACTORY FAILED! Make sure you have a no-arg constructor for class " + clz.getName());
                    return null;
                }
            }
        };
    }

    T build();
}
