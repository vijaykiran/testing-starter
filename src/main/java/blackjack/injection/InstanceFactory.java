package blackjack.injection;

import java.util.Stack;

/**
 * A factory with nestable, thread-local scope.
 *
 * A factory should be declared as a static member of some well-known class (e.g. Dependencies) and given a function
 * (BuildFunction) for generating a default value.
 *
 * Users of that dependency can rely on the default, or they can override it for the executing thread and scope using
 * override, which will install a thread-local function for generating future values on that thread. The factory is a Java
 * AutoCloseable (via the Resettable interface), meaning that you can use it with try in order to ensure such an
 * override does not "leak" outside of the desired context:
 *
 * <pre>
 *     try(Resettable r = Dependencies.myFactory.override(() => ...)) {
 *         Dependencies.myFactor.make(); // generates using the overridden builder
 *         f(); // code executed in this block that does not further override it also use the new builder
 *     }
 *     f(); // Uses the old builder
 * </pre>
 *
 * The override is implemented with a stack, so that nested overrides act as one would expect from a local reasoning standpoint.
 *
 * This nested behavior is particularly handy in production code where a code path might indicate a change in policy. For example,
 * one might have an incoming request where the code will check security settings and then set dependency injectors to
 * inject security-limited (or open) instances of things into that thread's environment for the duraction of that request. The
 * try block would automatically reset such injections under all circumstances, ensuring against leaky abstractions and security.
 * @param <T>
 */
public class InstanceFactory<T> implements Resettable {
    private final BuildFunction<T> builder;
    private ThreadLocal<Stack<BuildFunction<T>>> overrideBuilder = new ThreadLocal<Stack<BuildFunction<T>>>() {
        @Override
        protected Stack<BuildFunction<T>> initialValue() {
            return new Stack<BuildFunction<T>>();
        }
    };

    public InstanceFactory(BuildFunction<T> builder) {
        this.builder = builder;
    }

    /**
     * Make an instance via the current build function of this factory.
     */
    public T make() {
        if (overrideBuilder.get().isEmpty())
            return builder.build();
        return overrideBuilder.get().peek().build();
    }

    /**
     * Push the given builder onto the thread local override stack (calls may be nested). Future calls of make() on this
     * factory from the calling thread will use this new build function. Returns an AutoCloseable instance that can be used with a try
     * block to ensure that this override is also local to a given block of code.
     *
     * Calling close() on the return value will undo this override.
     *
     * @param builder The new build function to user.
     * @return A Resettable (AutoCloseable) that can be used to undo the override.
     */
    public Resettable override(BuildFunction<T> builder) {
        this.overrideBuilder.get().push(builder);
        return this;
    }

    /**
     * Close the current override. Pops the most recent thread-local builder override on this factory. Is a no-op if there
     * are no overrides in the calling thread's context.
     */
    @Override
    public void close() {
        if (!overrideBuilder.get().isEmpty())
            overrideBuilder.get().pop();
    }
}

