package blackjack;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

public class SystemConsole implements ConsoleIO {
    private final static LineNumberReader in = new LineNumberReader(new InputStreamReader(System.in));

    public SystemConsole() {
    }

    @Override
    public String readLine() {
        try {
            return in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void printf(String format, Object... args) {
        System.out.printf(format, args);
    }
}
