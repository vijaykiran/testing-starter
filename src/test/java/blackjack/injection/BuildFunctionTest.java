package blackjack.injection;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class BuildFunctionTest {
    public static  class X {
        public X() {}
    }

    @Test
    public void singletonBuilder__creates_a_build_function_that_return_the_same_cached_instance_every_time() {
        final BuildFunction<X> f = BuildFunction.singletonBuilder(X.class);
        X a = f.build();
        X b = f.build();

        assertNotNull(a);
        assertTrue("are the same instance", a == b);
    }
}
