package blackjack.injection;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InstanceFactoryTest {
    @Test
    public void requires_a_builder_function_which_produces_the_value() {
        InstanceFactory<Integer> factory = new InstanceFactory<>(() -> 42);

        int actual = factory.make();

        assertEquals(42, actual);
    }

    @Test
    public void allows_user_to_override_lambda_in_a_try_with_resources() {
        InstanceFactory<Integer> factory = new InstanceFactory<>(() -> 42);
        try (Resettable r = factory.override(() -> 12)) {
            assertEquals(12, factory.make().longValue());
        }
    }

    @Test
    public void resumes_original_value_after_override_in_try_with_resources() {
        InstanceFactory<Integer> factory = new InstanceFactory<>(() -> 42);
        try (Resettable r = factory.override(() -> 12)) {
        }
        assertEquals(42, factory.make().longValue());
    }

    private Runnable holdValueAt12(Object barrier, InstanceFactory<Integer> factory) {
        return new Runnable() {
            @Override
            public void run() {
                try (Resettable r = factory.override(() -> 12)) {
                    synchronized (barrier) {
                        barrier.notifyAll();
                    }
                    Thread.sleep(500);
                } catch (Exception e) {
                }
            }
        };
    }

    @Test
    public void allows_thread_local_override_of_value() throws InterruptedException {
        final Object barrier = new Object();
        InstanceFactory<Integer> factory = new InstanceFactory<>(() -> 42);

        Thread t2 = new Thread(holdValueAt12(barrier, factory));

        synchronized (barrier) {
            t2.start();
            barrier.wait(1500L);
            assertEquals(42, factory.make().longValue());
        }
    }

    @Test
    public void overrides_nest_in_a_try_with_resources_scope() {
        InstanceFactory<Integer> factory = new InstanceFactory<>(() -> 1);
        try (Resettable r = factory.override(() -> 2)) {
            try (Resettable r2 = factory.override(() -> 3)) {
                assertEquals(3, factory.make().longValue());
            }
            assertEquals(2, factory.make().longValue());
        }
        assertEquals(1, factory.make().longValue());
    }
}
